const md5 = require('md5'),
	RoomActionsBase = require('./RoomActionsBase');

class Room {
	constructor(RoomActions) {
		this.nonce = Math.random();
		this.mates = {};
		this.history = [];
		this.replyTimeout = null;

		this.roomActions = new RoomActions(logPiece => this.updateHistory(logPiece), () => this.getMatesArray());
		if (!(this.roomActions instanceof RoomActionsBase)) {
			throw new Error('new Room(<some class extends RoomActionsBase>)');
			return;
		}
	}
	canJoin() {
		return this.roomActions.canJoin(this.getMatesArray());
	}
	getMate(key) {
		return this.mates[key];
	}
	genKey(nickname) {
		return md5('' + this.nonce + nickname + Date.now() + Math.random());
	}
	getUniqueNickname(nickname) {
		let number = '',
			mates = this.getMatesArray();
		while (mates.find(m => m.nickname === nickname + number)) {
			number = number || 0;
			number++;
		}

		return nickname + number;
	}
	replyToAll() {
		this.replyTimeout = null;
		for (let i in this.mates) {
			let mate = this.mates[i];
			if (mate.reply !== null) {
				if (mate.updateFrom < this.history.length) {
					let updateForThatUser = this.history.slice(mate.updateFrom, this.history.length);
					mate.reply(updateForThatUser);
					mate.reply = null;
					mate.updateFrom = this.history.length;
				}
			} else if (mate.updateFrom < this.history.length - 1) {
				this.mateLeave(i);
			}
		}
	}
	updateHistory(logPiece) {
		this.history.push(logPiece);
		if (this.replyTimeout === null) {
			this.replyTimeout = setTimeout(() => this.replyToAll(), 1);
		}
	}
	mateJoin(nickname) {
		let key = this.genKey(nickname),
			uniqueNickname = this.getUniqueNickname(nickname);
		
		this.mates[key] = {
			nickname: uniqueNickname,
			reply: null,
			updateFrom: this.history.length
		};

		let args = [...arguments];
		args[0] = this.mates[key];

		let response = this.roomActions.onMateJoin.apply(this.roomActions, args);

		response.body = response.body || {};
		response.body.key = key;
		response.body.yourNickname = uniqueNickname;
		return response;
	}
	mateLeave(key) {
		let mate = this.mates[key];
		delete this.mates[key];
		this.roomActions.onMateLeave(mate);
	}
	action(action, roomId, key) {
		let args = [...arguments];
		args.shift();
		args.shift();
		args[0] = this.mates[key];

		return this.roomActions[action].apply(this.roomActions, args);;
	}
	hasAction(action) {
		return this.roomActions.actions[action] && typeof(this.roomActions[action]) === 'function';
	}
	getMatesArray() {
		let arr = [];
		for (let i in this.mates) {
			arr.push(this.mates[i]);
		}
		return arr;
	}
}

module.exports = Room;
