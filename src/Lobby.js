const md5 = require('md5'),
	Room = require('./Room'),
	RoomActionsBase = require('./RoomActionsBase'),
	LobbyCodes = require('./LobbyCodes');

class Lobby {
	constructor(roomActionsClass) {
		this.rooms = {};
		this.nonce = Math.random();
		this.roomActionsClass = roomActionsClass;
	}
	getNextRoomId() {
		return md5('' + this.nonce + Date.now());
	}
	getRoomURI(roomId) {
		return '/' + roomId;
	}
	withRoom(roomId, func) {
		if (!this.rooms[roomId]) {
			return {
				code: LobbyCodes.RoomNotFound
			};
		}
		return func(this.rooms[roomId]);
	}
	withMate(room, key, func) {
		let mate = room.getMate(key);
		if (!mate) {
			return {
				code: LobbyCodes.MateNotFound
			};
		}
		return func(mate);
	}
	hasRoom(roomId) {
		return this.rooms[roomId];
	}
	async findRoom() {
		let id = null;
		for (let r in this.rooms) {
			if (this.rooms[r].canJoin()) {
				id = r;
				break;
			}
		}

		if (!id) {
			id = this.getNextRoomId();
			if (!id) {
				return {
					code: LobbyCodes.MaxRoomsReached
				};
			}
			this.rooms[id] = new Room(this.roomActionsClass);
		}

		return {
			code: LobbyCodes.RedirectToRoom,
			head: {
				Location: this.getRoomURI(id)
			}
		};
	}
	async joinRoom(roomId, nickname) {
		if (typeof(roomId) !== 'string' || typeof(nickname) !== 'string') {
			throw new Error('Lobby::joinRoom(string, string, ...)');
		}
		return this.withRoom(roomId, room => {
			if (!room.canJoin()) {
				return {
					code: LobbyCodes.CantJoinRoom
				};
			}

			let args = [...arguments];
			args.shift();
			return room.mateJoin.apply(room, args);
		});
	}
	async getUpdates(roomId, key) {
		if (typeof(roomId) !== 'string' || typeof(key) !== 'string') {
			throw new Error('Lobby::getUpdates(string, string)');
		}
		return this.withRoom(roomId, room => {
			return this.withMate(room, key, mate => {
				let resolve = null;
				let p = new Promise((res, rej) => {
					resolve = body => res({
						code: LobbyCodes.Ok,
						body
					});
				});
				mate.reply = resolve;
				room.replyToAll();
				return p;
			});
		});
	}
	async action(action, roomId, key) {
		if (typeof(roomId) !== 'string' || typeof(key) !== 'string' || typeof(action) !== 'string') {
			throw new Error('Lobby::action(string, string, string, ...)');
		}
		return this.withRoom(roomId, room => {
			return this.withMate(room, key, mate => {
				if (room.hasAction(action)) {
					return room.action.apply(room, arguments);
				} else {
					return {
						code: LobbyCodes.WrongAction,
					};
				}
			});
		});
	}
}

module.exports = Lobby;
