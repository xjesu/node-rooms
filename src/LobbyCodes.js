let LobbyCodes = {
	Ok: 200,
	RedirectToRoom: 307,
	RoomNotFound: 403,
	MateNotFound: 401,
	MaxRoomsReached: 503,
	CantJoinRoom: 501,
	WrongAction: 400,
	Unsupported: 404
};

LobbyCodes.getDescription = code => {
	for (let i in LobbyCodes) {
		if (LobbyCodes[i] === code) {
			return i;
		}
	}
	return 'Unknown';
};

module.exports = LobbyCodes;
