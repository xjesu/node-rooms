const LobbyCodes = require('./LobbyCodes');

class RoomActionsBase {
	constructor(log, getAllMates) {
		if (typeof(log) !== 'function') {
			throw new Error('new <class extends RoomActionsBase(function log(Object message) {...})>');
		}

		this.log = log;
	}
	canJoin(mates) {
		return mates.length < 10;
	}
	onMateJoin(mate) {
		this.log({
			type: 'join',
			nickname: mate.nickname
		});
		return {
			code: LobbyCodes.Ok
		};
	}
	onMateLeave(mate) {
		this.log({
			type: 'leave',
			nickname: mate.nickname
		});
	}
	onAction(mates, actionMate, action) {
		this.log({
			type: action,
			nickname: mate.nickname
		});
		return {
			code: LobbyCodes.Ok
		};
	}
}

module.exports = RoomActionsBase;
