# node-rooms #

A layer between your backend point (express/swagger/etc) and your app logic.

	cosnt rooms = require('node-rooms');
	const MyAppLogicsClass = require('./myAppLogics');
	const lobby = new rooms.Lobby(MyAppLogicsClass);
	app.get('/', (req, res) => {
		lobby.findRoom().then(result => {
			res.status(result.code);
			res.send(JSON.stringify(result.data));
		});
	});

### Lobby methods (all async):

	lobby.findRoom()
returns an URI of random available room

	lobby.joinRoom(roomId, nickname, ...)
returns an object with a key, fixed nickname and something else
override MyAppLogicsClass.onMateJoin() to extend

	lobby.getUpdates(roomId, key)
delayed reply with room history slice: joins, leaves, actions etc
triggers when it needs

	lobby.action(roomId, key, action, ...)
any user action - maybe message or game turn

Check errors with `result.code !== LobbyCodes.Ok`

### MyAppLogicsClass
needs be extended from RoomActionsBase
