let NodeRooms = require('../');

class SampleRoomActions extends NodeRooms.RoomActionsBase {
	constructor(log, getAllMates) {
		super(log);

		this.getAllMates = getAllMates;
		this.scores = {};
		this.currentAnswer = null;
		this.timeout = this.newQuestion();
		this.actions = {
			answer: true
		};
	}
	onMateJoin(mate) {
		/*if (this.getAllMates().length > 1) {
			if (this.timeout === null) {
				this.timeout = this.newQuestion();
			}
		}*/
		return super.onMateJoin(mate);
	}
	newQuestion(timer) {
		if (!timer) {
			if (this.currentAnswer !== null) {
				this.log({
					type: 'answer',
					message: 'Correct answer was ' + this.currentAnswer
				});
			}

			let number1 = (Math.random() * 10) | 0,
				number2 = (Math.random() * 10) | 0;

			this.currentAnswer = '' + (number1 * number2);
			this.log({
				type: 'question',
				message: number1 + ' * ' + number2 + ' = ?'
			});

			timer = 5;
		} else {
			this.log({
				type: 'time',
				message: (timer * 10) + ' seconds left'
			});
			timer--;
		}

		if (true){//this.getAllMates() > 0) {
			this.timeout = setTimeout((t) => this.newQuestion(t), 10000, timer);
		} else {
			this.timeout = null;
		}
	}
	answer(mate, answer) {
		if (answer === this.currentAnswer) {
			this.log({
				type: 'answer',
				message: 'Player ' + mate.nickname + ' is right!'
			});
			clearTimeout(this.timeout);
			this.timeout = this.newQuestion();
		} else {
			this.log({
				type: 'answer',
				message: 'Player ' + mate.nickname + ' answer "' + answer + '" is wrong'
			});
		}
		return {
			code: NodeRooms.LobbyCodes.Ok
		};
	}
}

module.exports = SampleRoomActions;
