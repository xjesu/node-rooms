let NodeRooms = require('../'),
	SampleRoomActions = require('./SampleRoomActions'),
	http = require("http"),
	fs = require("fs");

let ifaces = require('os').networkInterfaces();

for (var ifname in ifaces) {
	for (var i = 0; i < ifaces[ifname].length; i++) {
		if (ifaces[ifname][i].family === 'IPv4') {
			console.log(ifname + ' ' + ifaces[ifname][i].address + ':' + 8080)
		}
	}
}

let lobby = new NodeRooms.Lobby(SampleRoomActions);

function reply(response, obj) {
	obj.head = obj.head || {};
	obj.head['Content-Type'] = obj.head['Content-Type'] || 'application/json';
	response.writeHead(obj.code, obj.head);
	if (obj.head['Content-Type'] === 'application/json') {
		response.write(obj.body ? JSON.stringify(obj.body) : NodeRooms.LobbyCodes.getDescription(obj.body));
	} else if (obj.head['Content-Type'] === 'text/html') {
		response.write(obj.body);
	}
	response.end();
}

http.createServer((req, res) => {
	let query = req.url.split('/');
	if (!query[1]) {
		query[1] = 'findRoom';
	}

	if (lobby[query[1]]) {
		lobby[query[1]](query[2], query[3], query[4], query[5]).then(obj => reply(res, obj));
	} else if (lobby.hasRoom(query[1])) {
		reply(res, {
code: 200,
head:{
	'Content-Type': 'text/html'
},
body: `
<!DOCTYPE html>
<html>
	<head>
		<title>Test</title>
	</head>
	<body>
		<input id="inp" type="text" placeholder="nickname">
		<div id="log"></div>
		<script type="text/javascript">
			let inp = document.getElementById('inp'),
				log = document.getElementById('log'),
				roomId = window.location.pathname.substr(1),
				key = null;
			function getUpdates() {
				fetch('/getUpdates/' + roomId + '/' + key).then(response => response.json()).then(obj => {
					for (let u of obj) {
						let div = document.createElement('div');
						div.innerHTML = '<div>' + JSON.stringify(u) + '</div>';
						log.insertBefore(div, log.firstChild);
					}
					getUpdates();
				});
			}
			inp.onchange = () => {
				if (!key) {
					fetch('/joinRoom/' + roomId + '/' + inp.value).then(response => response.json()).then(obj => {
						key = obj.key;
						getUpdates();
						inp.placeholder = 'answer';
					});
				} else {
					fetch('/action/answer/' + roomId + '/' + key + '/' + inp.value);
				}
				inp.value = '';
			}
		</script>
	</body>
</html>
`});
	} else {
		reply(res, {
			code: NodeRooms.LobbyCodes.RedirectToRoom,
			head: {
				'Location': '/'
			}
		})
	}
}).listen(8080);
